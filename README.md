# ImPayingAttention
The Python script no body asked for but I made anyways. 

### What?
A simple script designed to sit on a computer during "Online Learning" that listens to the instructor. You can configure it to identify words and phrases over audio and respond via Slack messages. In other words, it's perfect for acting like you are paying attention!

I may use it while Penetration Testing my own server infrastructure during Cybersecurity classes, oh the irony.

### Why?
If you have to ask that question then you are more responsible than me and I applaud you.

### How Can I Trust You?
Well, if you don't know basic Python, you can't. You also wouldn't be able to use it anyways.

### How Do I Use It?
1. First, find a way to retrieve your Slack token. Good luck, because Slack deprecated personal access tokens and I bet instructors won't let you install custom apps on their Slack instances. And no, you cant just inspect element, search for "Token", and pull that sucker out. Tried it, failed it.... for 3 hours.
2. Either modify the script or make that token the environment variable "SLACK_ACCESS_TOKEN." Duh.
3. Modify the script to use a channel of your choosing.
4. Modify the configuration template and rename it to "config.json." Figure out the convention, first entry in the template has all the options. FYI, the logic for them stacks.
5. Figure out what "pip install" commands you need to run. Who needs a "requirements.txt" when Google is your friend?
6. Figure out how to funnel your speaker audio back into your computer as an audio input.
7. Profit $$$.

### Can't You Make It Easier
Yes, but then every class that also happens to use Slack exclusively for communication could be riddled with bots. And then I'm no longer unique. And the only true programmers are masochists. Hit me UDP, hit me. You'll never know if it hurt. I'm sorry did I offend you? I wrote an apology letter but I can't tell if you got that either. At least everyone uses you to add their own...

### Letter To UDP
layers of abstraction on top of you, still with the end goal of knowing if their packet reached the destination. You're just the bottom UDP and everyone goes out of their way to avoid TCP for real time applications but you're still not good enough. Ever hear about the "Two Generals" problem? That's a problem with TCP but at least they try to reach concurrency. You just fire off blindly into the dark hoping that someone else hears you. But you'll never know, will you?