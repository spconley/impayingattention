import speech_recognition as sr
import requests
import json
import os
import random

# Holy f*** this is a mess
# I must be Italian because this sure is spaghetti

# I may be stupid but I'm not putting my ACCESS_TOKEN in my program
# Piss off Git crawlers, go attack hospitals since they love to leave passwords in source
ACCESS_TOKEN = os.environ.get('SLACK_ACCESS_TOKEN')

DEBUG_MODE = True
NAME = []
JSON_DATA = {}



def main():
	global NAME, JSON_DATA
	with open('config.json', 'r') as json_file:
		JSON_DATA = json.loads(json_file.read())

	NAME = JSON_DATA['names']

	r = sr.Recognizer()
	r.pause_threshold = 1
	mic = sr.Microphone()
	
	# Run Forest, Run
	try:
		while True:
			listen(r, mic)

	except KeyboardInterrupt:
		pass

# Just listen to the mic like the NSA
def listen(r, mic):

	with mic as source: 
		audio = r.record(source=source, duration=20) 

	try:
		result = r.recognize_google(audio).lower()
		botResponses(result)

	except sr.UnknownValueError:
		print('[Error] Could not understand the audio')

	except sr.RequestError as e:
		print('[Error] Could not request results: {0}'.format(e))

# Just detect some words and give a witty response
def botResponses(result):
	global DEBUG_MODE, JSON_DATA

	# Loop through all those situation objects
	for obj in JSON_DATA['situations']:

		conditionsMet = 0
		conditionsNeeded = 0

		# If name is required, check for it
		# If name requirement doesn't exist, no sweat
		# Similar process for all below fields
		if 'requireName' in obj:
			if obj['requireName'] == "True":
				if any(x in result for x in NAME):
					conditionsMet += 1

				conditionsNeeded += 1

		if 'anyMatch' in obj:
			if obj['anyMatch']:
				if any(x in result for x in obj['anyMatch']):
					conditionsMet += 1

				conditionsNeeded += 1

		if 'allMatch' in obj:
			if obj['allMatch']:
				if all(x in result for x in obj['allMatch']):
					conditionsMet += 1

				conditionsNeeded += 1

		# If litterarly nothing matched, somehow, ignore it 
		if conditionsMet == 0:
			continue

		# If all the provided conditions are met, sweet
		if conditionsMet == conditionsNeeded:
			print('[INFO] Match Found')
			rLength = len(obj['responses'])
			sendMessage(obj['responses'][random.randint(0, rLength - 1)], channel='general')

		
	
	if DEBUG_MODE:
		print('[Debug] Result:', result)

# Just get the payload and hit em up
def sendMessage(message, result='', channel='general'):
	global ACCESS_TOKEN, DEBUG_MODE
	url='https://slack.com/api/chat.postMessage'
	headers={'Content-type':'application/json; charset=utf-8', 'Authorization': ('Bearer ' + ACCESS_TOKEN)}

	payload = {'channel':channel, 'text':message}
	response = requests.post(url, headers=headers, data=json.dumps(payload))
	if (DEBUG_MODE):
		print('[Debug] Post:\nURL=', url, '\nHEADERS=', headers, '\nPAYLOAD=', payload)
		print('[Debug] Response:', response.content)



if __name__ == "__main__":
	main()